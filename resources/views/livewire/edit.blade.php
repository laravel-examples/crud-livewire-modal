<!-- Modal -->
<div wire:ignore.self class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editModalLabel">Editar Post</h5>
                <button wire:click="default" type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                @include('livewire.form')
            </div>
            <div class="modal-footer">
                <button wire:click="default" type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button wire:click="update" type="button" class="btn btn-primary">Guardar</button>
            </div>
        </div>
    </div>
</div>
