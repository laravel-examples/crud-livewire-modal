<h2>Listado de Posts</h2>

<!-- Button trigger modal -->
<div class="form-group">
    <button wire:click="default" type="button" class="btn btn-success" data-toggle="modal" data-target="#createModal">
        New Post
    </button>
</div>

<div class="input-group mb-3">
    <div class="input-group-prepend">
        <span class="input-group-text" id="basic-addon3">Buscar</span>
    </div>
    <input type="text" class="form-control" placeholder="Título o contenido" wire:model="search">
    <select class="form-control" wire:model="perPage">
        <option value="3">3 por página</option>
        <option value="5">5 por página</option>
        <option value="10">10 por página</option>
        <option value="15">15 por página</option>
        <option value="20">20 por página</option>
    </select>
    @if ($search !== '')
    <button wire:click="clear" class="btn btn-light">
        X
    </button>
    @endif
</div>

<table class="table">
    <thead>
        <tr>
            <th>ID</th>
            <th>Título</th>
            <th>Contenido</th>
            <th colspan="2">&nbsp;</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($posts as $post)
        <tr>
            <td>{{ $post->id }}</td>
            <td>{{ $post->title }}</td>
            <td>{{ $post->body }}</td>
            <td>
                <button wire:click="edit({{ $post->id }})" class="btn btn-primary" data-toggle="modal" data-target="#editModal">
                    Editar
                </button>
            </td>
            <td>
                <button wire:click="destroy({{ $post->id }})" class="btn btn-danger">
                    Eliminar
                </button>
            </td>
        </tr>
        @empty
        <tr>
            <td class="text-muted" colspan="5">No hay resultados para la búsqueda <strong>{{ $search }}</strong> en la página {{ $page }} al mostrar {{ $perPage }} por página.</td>
        </tr>
        @endforelse
    </tbody>
</table>

<div class="row">
    <div class="col text-muted">
        Mostrando {{ $posts->firstItem() }} a {{ $posts->lastItem() }} de {{ $posts->total() }} resultados
    </div>
    <div class="col text-right">
        {{ $posts->links() }}
    </div>
</div>
