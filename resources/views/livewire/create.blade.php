<!-- Modal -->
<div wire:ignore.self class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="createModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="createModalLabel">Nuevo Post</h5>
                <button wire:click="default" type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                @include('livewire.form')
            </div>
            <div class="modal-footer">
                <button wire:click="default" type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button wire:click="store" type="button" class="btn btn-primary">Guardar</button>
            </div>
        </div>
    </div>
</div>
