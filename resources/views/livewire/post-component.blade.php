<div class="row">
    <div class="col-sm-3">
        @include('livewire.create')
        @include('livewire.edit')
    </div>
    <div class="col-sm-9">
        @include('livewire.table')
    </div>
</div>
