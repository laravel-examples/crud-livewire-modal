@extends('layout')

@section('content')
    <div class="container">
        @livewire('post-component')
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        window.livewire.on('closeModal', () => {
            $('.modal').modal('hide');
        });
    </script>
@endsection
