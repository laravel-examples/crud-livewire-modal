<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use App\Post;

class PostComponent extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';
    protected $queryString = [
        'page' => ['except' => 1],
        'perPage' => ['except' => '3'],
        'search' => ['except' => '']
    ];

    public $post_id, $title, $body;
    public $perPage = '3';
    public $search = '';

    public function render()
    {
        return view('livewire.post-component', [
            'posts' => Post::where('title', 'like', "%{$this->search}%")
                       ->orWhere('body', 'like', "%{$this->search}%")
                       ->orderBy('id', 'desc')->paginate($this->perPage)
        ]);
    }

    public function store()
    {
        $data_validated = $this->validate([
            'title' => 'required',
            'body' => 'required'
        ]);
        Post::create($data_validated);
        $this->emit('closeModal');
    }

    public function edit($id)
    {
        $this->resetValidation();
        $post = Post::find($id);
        $this->post_id = $post->id;
        $this->title = $post->title;
        $this->body = $post->body;
    }

    public function update()
    {
        $data_validated = $this->validate([
            'title' => 'required',
            'body' => 'required'
        ]);
        $post = Post::find($this->post_id);
        $post->update($data_validated);
        $this->emit('closeModal');
        $this->default();
    }

    public function destroy($id)
    {
        Post::destroy($id);
    }

    public function default()
    {
        $this->resetValidation();
        $this->post_id = '';
        $this->title = '';
        $this->body = '';
    }

    public function clear()
    {
        $this->search = '';
        $this->page = 1;
        $this->perPage = '3';
    }
}
